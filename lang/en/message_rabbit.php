<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'message_rabbit', language 'en'
 *
 * @package    message_rabbit
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['configrabbithost'] = 'The server to connect to to send rabbit message notifications';
$string['configrabbitvhost'] = 'Virtual host ID (can be left empty if the same as rabbit host)';
$string['configrabbitusername'] = 'The user name to use when connecting to the rabbit server';
$string['configrabbitpassword'] = 'The password to use when connecting to the rabbit server';
$string['configrabbitport'] = 'The port to use when connecting to the rabbit server';
$string['configrabbitexchange'] = 'The exchange name to use when send message';
$string['configrabbitdeliverymode'] = 'The exchange mode to send message';
$string['configrabbitdurable'] = 'The message is durable or not, select 0 or 1 to disable or enable';
$string['rabbithost'] = 'rabbit host';
$string['rabbitid'] = 'rabbit ID';
$string['rabbitvhost'] = 'rabbit vhost';
$string['rabbitusername'] = 'rabbit user name';
$string['rabbitpassword'] = 'rabbit password';
$string['rabbitport'] = 'rabbit port';
$string['rabbitexchange'] = 'exchange name';
$string['rabbitdeliverymode'] = 'exchange mode';
$string['rabbitdurable'] = 'durable';
$string['notconfigured'] = 'The rabbit server hasn\'t been configured so rabbit messages cannot be sent';
$string['pluginname'] = 'rabbit message';
