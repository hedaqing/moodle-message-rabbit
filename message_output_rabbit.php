<?php
require_once($CFG->dirroot.'/message/output/lib.php');
require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class message_output_rabbit extends message_output {

    function send_message($eventdata){
        global $CFG, $DB;
        // Skip any messaging of suspended and deleted users.

        // if ($eventdata->userto->auth === 'nologin' or $eventdata->userto->suspended or $eventdata->userto->deleted) {
        if ($eventdata->userto->suspended or $eventdata->userto->deleted) {
            return true;
        }

        if (PHPUNIT_TEST) {
            // No connection to external servers allowed in phpunit tests.
            return true;
        }

        //hold onto rabbit id preference because /admin/cron.php sends a lot of messages at once
        static $rabbitaddresses = array();
        if (!array_key_exists($eventdata->userto->id, $rabbitaddresses)) {
            $userto = $DB->get_record('user', array('id'=>$eventdata->userto->id));
            $rabbitaddresses[$eventdata->userto->id] = $userto->idnumber;
        }
        $rabbitaddress = $rabbitaddresses[$eventdata->userto->id];

        // get message body
        $rabbitmessage = array();
        $rabbitmessage['fromApp'] = 'moodle';
        $rabbitmessage['msgType'] = $eventdata->name;
        $rabbitmessage['content'] = ltrim(rtrim($eventdata->smallmessage,"<\/p>"),"<p>");
        $rabbitmessage['badgeNumber'] = $rabbitaddress;
        $msg = json_encode($rabbitmessage);
        $message = new AMQPMessage($msg);        

        $connection = new AMQPStreamConnection($CFG->rabbithost, $CFG->rabbitport, $CFG->rabbitusername, $CFG->rabbitpassword, $CFG->rabbitvhost);
        $channel = $connection->channel();

        $channel->exchange_declare($CFG->rabbitexchange,$CFG->rabbitdeliverymode , true, $CFG->rabbitdurable, false);

        $channel->basic_publish($message, $CFG->rabbitexchange);

        $channel->close();
        $connection->close();

        return true;
    }

    /**
     * Creates necessary fields in the messaging config form.
     *
     * @param array $preferences An array of user preferences
     */
    function config_form($preferences){
        // global $CFG;

        // if (!$this->is_system_configured()) {
           // return get_string('notconfigured','message_rabbit');
        // } else {
           // return get_string('rabbitid', 'message_rabbit').': <input size="30" name="rabbit_rabbitid" value="'.s($preferences->rabbit_rabbitid).'" />';
        // }
    }

    /**
     * Parses the submitted form data and saves it into preferences array.
     *
     * @param stdClass $form preferences form class
     * @param array $preferences preferences array
     */
    function process_form($form, &$preferences){
        // if (isset($form->rabbit_rabbitid) && !empty($form->rabbit_rabbitid)) {
           // $preferences['message_processor_rabbit_rabbitid'] = $form->rabbit_rabbitid;
        // }
    }

    /**
     * Loads the config data from database to put on the form during initial form display
     *
     * @param array $preferences preferences array
     * @param int $userid the user id
     */
    function load_data(&$preferences, $userid){
        // $preferences->rabbit_rabbitid = get_user_preferences( 'message_processor_rabbit_rabbitid', '', $userid);
    }

    /**
     * Tests whether the rabbit settings have been configured
     * @return boolean true if rabbit is configured
     */
    function is_system_configured() {
        global $CFG;
        return (!empty($CFG->rabbithost) && 
            !empty($CFG->rabbitport) && 
            !empty($CFG->rabbitusername) && 
            !empty($CFG->rabbitpassword) && 
            !empty($CFG->rabbitexchange) && 
            !empty($CFG->rabbitdurable));
    }

    /**
     * Tests whether the rabbit settings have been configured on user level
     * @param  object $user the user object, defaults to $USER.
     * @return bool has the user made all the necessary settings
     * in their profile to allow this plugin to be used.
     */
    function is_user_configured($user = null) {
        // global $USER;

        // if (is_null($user)) {
           // $user = $USER;
        // }
        // return (bool)get_user_preferences('message_processor_rabbit_rabbitid', null, $user->id);
	return true;
    }
}
